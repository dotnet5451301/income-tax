using IncomeTax.Core.Models;

namespace IncomeTax.Core.Repositories
{
	public interface IInsuranceRepository : IRepository<Insurance>
	{
	}
}