﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using IncomeTax.Core.Domain;
using IncomeTax.Core.Models;

namespace IncomeTax.Core.Repositories
{
	public interface IIdentityRepository : IRepository<User>
	{
		Task<IList<string>> GetUserRolesAsync(User user);

		Task<IList<Claim>> GetUserClaimsAsync(User user);

		Task<Result> ChangePasswordAsync(User user, string oldPassword, string newPassword);

		Task CreateUserWithPasswordHash(User user);

		Task<bool> HasValidPassword(User user, string password);

		Task<Result> AddToRoleAsync(User user, string role);

		Task<Result> AddToRolesAsync(User user, IList<string> roles);
	}
}