﻿using IncomeTax.Core.Models;

namespace IncomeTax.Core.Repositories
{
	public interface IRefreshTokenRepository : IRepository<RefreshToken> { }
}