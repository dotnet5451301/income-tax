﻿using IncomeTax.Core.Models;

namespace IncomeTax.Core.Repositories
{
	public interface IPersonRepository : IRepository<Person> { }
}