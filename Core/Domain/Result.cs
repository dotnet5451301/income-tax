﻿using System.Collections.Generic;

namespace IncomeTax.Core.Domain
{
	public class Result
	{
		public Result(bool succeed, string message, IList<Dictionary<string, string>> errors = null)
		{
			Succeed = succeed;
			Message = message;
			Errors = errors;
		}

		public bool Succeed { get; }
		public string Message { get; }
		public IList<Dictionary<string, string>> Errors { get; }
	}
}