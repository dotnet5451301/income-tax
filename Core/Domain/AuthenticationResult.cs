﻿using System;
using System.Collections.Generic;

namespace IncomeTax.Core.Domain
{
	public class AuthenticationResult
	{
		public bool Success { get; set; }
		public string Token { get; set; }
		public Guid RefreshToken { get; set; }
		public IDictionary<string, string> Errors { get; set; }
	}
}