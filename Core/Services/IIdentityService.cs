﻿using System;
using System.Threading.Tasks;
using IncomeTax.Core.Domain;
using IncomeTax.Core.Models;

namespace IncomeTax.Core.Services
{
	public interface IIdentityService
	{
		Task<AuthenticationResult> LoginAsync(string username, string password);

		Task<AuthenticationResult> RefreshTokenAsync(string token, Guid refreshToken);

		Task<Result> RegisterUserAsync(Person person);

		Task<Result> ChangePasswordAsync(User user, string oldPassword, string newPassword);
	}
}