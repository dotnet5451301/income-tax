using System.Threading.Tasks;
using IncomeTax.Core.Domain;
using IncomeTax.Core.Models;

namespace IncomeTax.Core.Services
{
	public interface IInsuranceService
	{
		Task<Result> AddAsync(long userId, Insurance insurance);
	}
}