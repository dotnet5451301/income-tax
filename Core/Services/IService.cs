﻿namespace IncomeTax.Core.Services
{
	public interface IService
	{
		IIdentityService Identity { get; }
		IInsuranceService Insurance { get; }
	}
}