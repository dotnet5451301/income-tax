﻿using System;
using System.Threading.Tasks;
using IncomeTax.Core.Repositories;

namespace IncomeTax.Core
{
	public interface IUnitOfWork : IDisposable
	{
		IIdentityRepository Users { get; }
		IRefreshTokenRepository RefreshTokens { get; }
		IPersonRepository Persons { get; }
		IInsuranceRepository Insurances { get; }

		Task<int> CompleteAsync();
	}
}