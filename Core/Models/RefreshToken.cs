﻿using System;
using Microsoft.AspNetCore.Identity;

namespace IncomeTax.Core.Models
{
	public class RefreshToken
	{
		public Guid Token { get; set; }
		public string JwtId { get; set; }
		public DateTime CreationDate { get; set; }
		public DateTime ExpiryDate { get; set; }
		public bool Used { get; set; }
		public bool Invalidated { get; set; }

		public long UserId { get; set; }
		public User User { get; set; }
	}
}