using System;

namespace IncomeTax.Core.Models
{
	public class Insurance : FixedAsset
	{
		public string PolicyNo { get; set; }
		public decimal Premium { get; set; }
		public decimal MaturityValue { get; set; }
		public DateOnly MaturityDate { get; set; }
	}
}