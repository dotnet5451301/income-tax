using System;

namespace IncomeTax.Core.Models
{
	public class CryptoCurrency : FixedAsset
	{
		public DateOnly? SaleDate { get; set; }
		public decimal? SaleValue { get; set; }
		public string CryptoName { get; set; }
		public float Quantity { get; set; }
	}
}