namespace IncomeTax.Core.Models
{
	public class BankAccount : CurrentAsset
	{
		/// <summary>
		///     The company where the investment has made. In case of a bank account, this is the name of the banking company.
		/// </summary>
		public string Bank { get; set; }

		public long AddressId { get; set; }
		public Address Address { get; set; }
		
		public string AccountNo { get; set; }
		public string Branch { get; set; }
		public byte AccountTypeId { get; set; }
		public AccountType Type { get; set; }
	}
}