using System;

namespace IncomeTax.Core.Models
{
	public class Liability
	{
		public long Id { get; set; }
		public byte LiabilityTypeId { get; set; }
		public LiabilityType Type { get; set; }
		public decimal Value { get; set; }
		public DateOnly Date { get; set; }

		/// <summary>
		/// Registration No. of the organization or person from whom the amount is borrowed.
		/// </summary>
		public string RegistrationNo { get; set; }

		/// <summary>
		/// Name of the organization or person from whom the amount is borrowed
		/// </summary>
		public string Name { get; set; }
		public long PersonId { get; set; }
		public Person Person { get; set; }
	}
}