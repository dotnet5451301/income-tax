namespace IncomeTax.Core.Models
{
	public class CashEquivalent : CurrentAsset
	{
		public decimal Value { get; set; }
		
		public byte NatureId { get; set; }
		public CashNature Nature { get; set; }
	}
}