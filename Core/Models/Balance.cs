using System;

namespace IncomeTax.Core.Models
{
	public class Balance
	{
		public DateOnly Date { get; set; }
		public decimal Value { get; set; }
		public bool IsProfit { get; set; }

		public long CurrentAssetId { get; set; }
		public virtual CurrentAsset Asset { get; set; }
	}
}