using Microsoft.AspNetCore.Identity;

namespace IncomeTax.Core.Models
{
	public class UserLogin : IdentityUserLogin<long>
	{
		public User User { get; set; }	
	}
}