using System;

namespace IncomeTax.Core.Models
{
	public class Debenture : FixedAsset
	{
		public string DebentureNo { get; set; }
		public float InterestRate { get; set; }
		
		/// <summary>
		/// Duration should be stored in Days but can be displayed in either months or years which can be handled at front-end
		/// </summary>
		public TimeSpan Duration { get; set; }
	}
}