using System;

namespace IncomeTax.Core.Models
{
	public abstract class FixedAsset : Asset
	{
		public DateOnly PurchaseDate { get; set; }
		public decimal PurchaseValue { get; set; }
	}
}