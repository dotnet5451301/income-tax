using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.AspNetCore.Identity;

namespace IncomeTax.Core.Models
{
	public class User : IdentityUser<long>
	{
		public User()
		{
			Logins = new Collection<UserLogin>();
			Claims = new Collection<UserClaim>();
			Tokens = new Collection<UserToken>();
			Roles = new Collection<UserRole>();
		}

		public IEnumerable<UserLogin> Logins { get; set; }
		public IEnumerable<UserClaim> Claims { get; set; }
		public IEnumerable<UserToken> Tokens { get; set; }

		public IEnumerable<UserRole> Roles { get; set; }
	}
}