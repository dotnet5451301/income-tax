namespace IncomeTax.Core.Models
{
	public class OtherCurrentAsset : CurrentAsset
	{
		public OtherAssetType OtherAssetType { get; set; }
		public byte OtherAssetTypeId { get; set; }
		public string Description { get; set; }
	}
}