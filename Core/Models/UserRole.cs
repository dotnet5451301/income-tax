using Microsoft.AspNetCore.Identity;

namespace IncomeTax.Core.Models
{
	public class UserRole : IdentityUserRole<long>
	{
		public User User { get; set; }
		public Role Role { get; set; }
	}
}