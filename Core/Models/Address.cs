namespace IncomeTax.Core.Models
{
	public class Address
	{
		public long Id { get; set; }
		public string StreetOne { get; set; }
		public string? StreetTwo { get; set; }
		public string? Area { get; set; }
		public string? City { get; set; }
		public bool IsCity { get; set; }
		public string? State { get; set; }
		public byte CountryId { get; set; }
		public Country Country { get; set; }
	}
}