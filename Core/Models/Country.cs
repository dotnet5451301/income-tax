using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace IncomeTax.Core.Models
{
	public class Country
	{
		public Country() => Addresses = new Collection<Address>();

		public byte Id { get; set; }
		public string Name { get; set; }

		public IEnumerable<Address> Addresses { get; set; }
	}
}