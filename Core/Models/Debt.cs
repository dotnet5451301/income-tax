namespace IncomeTax.Core.Models
{
	/// <summary>
	/// Debts of the user such as Prepayments, Advance, Deposits, Receivables, etc.
	/// </summary>
	public class Debt : CurrentAsset
	{
		/// <summary>
		/// Here the user specify the tax number of the individual from whom the money is receivable
		/// </summary>
		public string RegistrationNo { get; set; }

		/// <summary>
		/// Name of the company or an individual
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Any other details that the user would like to provide
		/// </summary>
		public string Description { get; set; }

		public byte NatureId { get; set; }
		public DebtNature Nature { get; set; }
	}
}