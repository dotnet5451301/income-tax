using Microsoft.AspNetCore.Identity;

namespace IncomeTax.Core.Models
{
	public class RoleClaim : IdentityRoleClaim<long>
	{
		public Role Role { get; set; }
	}
}