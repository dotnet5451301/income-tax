using System;

namespace IncomeTax.Core.Models
{
	public class OtherInvestment : FixedAsset
	{
		public string InvestedInCompany { get; set; }
		public string AccountNo { get; set; }
		public OtherInvestmentType Type { get; set; }
		public byte TypeId { get; set; }
		public string Description { get; set; }

		public decimal? AnnualReturn { get; set; }
		public DateOnly? Maturity { get; set; }
		
		public decimal? MaturityValue { get; set; }
	}
}