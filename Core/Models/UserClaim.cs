using Microsoft.AspNetCore.Identity;

namespace IncomeTax.Core.Models
{
	public class UserClaim : IdentityUserClaim<long>
	{
		public User User { get; set; }
	}
}