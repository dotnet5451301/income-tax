using Microsoft.AspNetCore.Identity;

namespace IncomeTax.Core.Models
{
	public class UserToken : IdentityUserToken<long>
	{
		public User User { get; set; }
	}
}