namespace IncomeTax.Core.Models
{
	public abstract class TypeBase
	{
		public byte Id { get; set; }
		public string Name { get; set; }
	}
}