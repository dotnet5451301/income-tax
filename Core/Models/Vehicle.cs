using System;

namespace IncomeTax.Core.Models
{
	public class Vehicle : FixedAsset
	{
		public DateOnly? SaleDate { get; set; }
		public decimal? SaleValueValue { get; set; }
		public string RegistrationNo { get; set; }
		public string Make { get; set; }
		public string Model { get; set; }
		public string Brand { get; set; }
		public bool IsCommercial { get; set; }

		public byte VehicleTypeId { get; set; }
		public VehicleType VehicleType { get; set; }
	}
}