using System;

namespace IncomeTax.Core.Models
{
	/// <summary>
	/// Representing Database table Expenses.
	/// </summary>
	public class Expense
	{
		/// <summary>
		/// Primary Key of the Expenses table.
		/// </summary>
		public long Id { get; set; }
		
		/// <summary>
		/// Type of the expense
		/// </summary>
		public byte ExpenseTypeId { get; set; }
		
		/// <summary>
		/// This description should rather be provided when user selects Others in the Expense Type.
		/// </summary>
		public string? Description { get; set; }
		
		/// <summary>
		/// Value of the expense
		/// </summary>
		public decimal Value { get; set; }
		
		/// <summary>
		/// Date when the expense occurred.
		/// </summary>
		public DateOnly Date { get; set; }

		/// <summary>
		/// Foreign Key referencing Persons Table for one person to many expenses relationship
		/// </summary>
		public long PersonId { get; set; }
		
		/// <summary>
		/// Navigation property for Person Table
		/// </summary>
		public Person Person { get; set; }

		/// <summary>
		/// Navigation property for Expense Type
		/// </summary>
		public ExpenseType Type { get; set; }
	}
}