using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace IncomeTax.Core.Models
{
	public abstract class CurrentAsset : Asset
	{
		public CurrentAsset() => Balances = new Collection<Balance>();

		public IEnumerable<Balance> Balances { get; set; }
	}
}