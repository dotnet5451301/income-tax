using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.AspNetCore.Identity;

namespace IncomeTax.Core.Models
{
	public class Role : IdentityRole<long>
	{
		public Role()
		{
			Claims = new Collection<RoleClaim>();
			Users = new Collection<UserRole>();
		}


		public IEnumerable<RoleClaim> Claims { get; set; }
		public IEnumerable<UserRole> Users { get; set; }
	}
}