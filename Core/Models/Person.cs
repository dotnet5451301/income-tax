﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace IncomeTax.Core.Models
{
	public class Person
	{
		public Person()
		{
			Assets = new Collection<Asset>();
			Expenses = new Collection<Expense>();
			Liabilities = new Collection<Liability>();
		}
		
		public long Id { get; set; }
		public string FirstName { get; set; }
		public string MiddleName { get; set; }
		public string LastName { get; set; }
		public string CNIC { get; set; }
		public string IRISPasswordHash { get; set; }
		public string FullName => FirstName + (string.IsNullOrEmpty(MiddleName) ? "" : " " + MiddleName) + " " + LastName;
		public User User { get; set; }
		public long UserId { get; set; }
		public IEnumerable<Asset> Assets { get; set; }
		public IEnumerable<Expense> Expenses { get; set; }
		public IEnumerable<Liability> Liabilities { get; set; }
	}
}