using System;

namespace IncomeTax.Core.Models
{
	public class Property : FixedAsset
	{
		public bool IsConstructed { get; set; }
		
		public DateOnly? SaleDate { get; set; }
		public decimal? SaleValueValue { get; set; }

		public byte AreaTypeId { get; set; }
		public AreaType AreaType { get; set; }
		
		public long AddressId { get; set; }
		public Address Address { get; set; }
	}
}