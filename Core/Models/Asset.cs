namespace IncomeTax.Core.Models
{
	public abstract class Asset
	{
		public long Id { get; set; }
		public bool IsForeign { get; set; }
		public bool IsOnName { get; set; }

		public long PersonId { get; set; }
		public Person Person { get; set; }
	}
}