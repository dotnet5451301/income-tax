namespace IncomeTax.Core.Enums
{
	public enum VehicleType
	{
		Bike,
		Car,
		Bus,
		Truck,
		Carry
	}
}