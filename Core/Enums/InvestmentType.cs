namespace IncomeTax.Core.Enums
{
	public enum InvestmentType
	{
		SavingCertificate,
		FixedDeposit,
		Insurance,
		Shares,
		Stock,
		Debentures,
		CryptoCurrencies
	}
}