namespace IncomeTax.Core.Enums
{
	public enum ExpenseType
	{
		Water,
		Electricity,
		Gas,
		Telephone,
		Mobile,
		Education,
		Functions,
		Taxes,
		Donations,
		Interest,
		VehicleMaintenance,
		Medical,
		Memberships,
		Grocery,
		Food,
		Snacks,
		Restaurant,
		Hotel,
		Rent,
		Travelling,
		Tickets
	}
}