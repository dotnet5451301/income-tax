namespace IncomeTax.Core.Enums
{
	public enum DebtNature
	{
		Prepayment,
		Receivable,
		Advance,
		Security,
		Debt,
		Deposit
	}
}