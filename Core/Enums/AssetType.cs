namespace IncomeTax.Core.Enums
{
	public enum AssetType
	{
		Cash = 1,
		Prepayment,
		Receivable,
		Property,
		Vehicle,
		Furniture,
		Investment,
		Inventory,
		Equipment,
		PrizeBonds,
		PreciousPossession,
		Capital,
		ProvidentFund,
		Gratuity,
		PensionFund
	}
}