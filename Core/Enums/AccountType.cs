namespace IncomeTax.Core.Enums
{
	public enum AccountType
	{
		Savings = 1,
		Current,
		ProfitNLoss
	}
}