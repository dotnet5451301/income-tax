﻿namespace IncomeTax.Core.Enums
{
	public enum Role
	{
		/// <summary>
		///     The creators of the application.
		/// </summary>
		Administrator = 1,

		/// <summary>
		///     The Income Tax Consultant who'd be handling clients using this application
		/// </summary>
		Consultant,

		/// <summary>
		///     The role of the client which will be using this application for management of schedules.
		/// </summary>
		Client
	}
}