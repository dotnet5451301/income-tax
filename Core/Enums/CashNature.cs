namespace IncomeTax.Core.Enums
{
	public enum CashNature
	{
		Cash,
		Cheque,
		Bond,
		Security,
		PayOrder
	}
}