namespace IncomeTax.Core.Enums
{
	public enum OtherAssetType
	{
		PersonalItem = 1,
		Cash,
		Other
	}
}