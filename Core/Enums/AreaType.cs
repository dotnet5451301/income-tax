namespace IncomeTax.Core.Enums
{
	public enum AreaType
	{
		Residential,
		Commercial,
		Agriculture,
		Industrial
	}
}