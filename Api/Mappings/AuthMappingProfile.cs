﻿using IncomeTax.Api.Contracts.Auth.Responses;
using IncomeTax.Core.Domain;
using AutoMapper;

namespace IncomeTax.Api.Mappings
{
    public class AuthMappingProfile : Profile
    {
        public AuthMappingProfile()
        {
            CreateMap<AuthenticationResult, AuthSuccessResponse>();
            CreateMap<AuthenticationResult, AuthFailedResponse>();
        }
    }
}