﻿using IncomeTax.Api.Contracts.RequestModels;
using IncomeTax.Core.Models;
using AutoMapper;

namespace IncomeTax.Api.Mappings
{
    public class AddressMappingProfile : Profile
    {
        public AddressMappingProfile()
        {
            CreateMap<AddressRequest, Address>()
                .ReverseMap();
        }
    }
}