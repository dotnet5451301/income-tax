using AutoMapper;
using IncomeTax.Api.Contracts.Insurance;
using IncomeTax.Core.Models;

namespace IncomeTax.Api.Mappings
{
	public class InsuranceMappingProfile : Profile
	{
		public InsuranceMappingProfile()
		{
			CreateMap<InsuranceRequest, Insurance>()
				.ReverseMap();
		}
	}
}