﻿using System;
using IncomeTax.Api.Contracts.Auth.Requests;
using IncomeTax.Core.Models;
using AutoMapper;

namespace IncomeTax.Api.Mappings
{
	public class RegisterMappingProfile : Profile
	{
		public RegisterMappingProfile()
		{
			CreateMap<RegisterRequest, Person>()
				.ForMember(d => d.FirstName, o =>
					o.MapFrom(s => s.PersonalInformation.FirstName))
				.ForMember(d => d.MiddleName, o =>
					o.MapFrom(s => s.PersonalInformation.MiddleName))
				.ForMember(d => d.LastName, o =>
					o.MapFrom(s => s.PersonalInformation.LastName))
				.ForMember(d => d.CNIC, o =>
					o.MapFrom(s => s.PersonalInformation.CNIC))
				.ForMember(d => d.IRISPasswordHash, o =>
					o.MapFrom(s => s.PersonalInformation.IRISPassword))
				.ForMember(d => d.User, o =>
					o.MapFrom(s => s.User))
				.ForMember(d => d.UserId, o =>
					o.Ignore())
				.ForMember(d => d.Id, o =>
					o.Ignore());
		}
	}
}