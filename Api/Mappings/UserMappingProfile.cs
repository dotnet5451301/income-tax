using AutoMapper;
using IncomeTax.Api.Contracts.RequestModels;
using IncomeTax.Core.Models;
using Microsoft.AspNetCore.Identity;

namespace IncomeTax.Api.Mappings
{
	public class UserMappingProfile : Profile
	{
		public UserMappingProfile()
		{
			CreateMap<UserRequest, User>()
				.ForMember(d => d.TwoFactorEnabled, o =>
					o.MapFrom(s => s.TwoFactorAuth))
				.ForMember(d => d.PasswordHash, o =>
					o.MapFrom(s => s.Password))
				.ForMember(d => d.PhoneNumber, o =>
					o.MapFrom(s => s.Phone))
				.ForMember(d => d.Email, o =>
					o.MapFrom(s => s.Email.ToLower()));
		}
	}
}