﻿using System.Threading.Tasks;
using AutoMapper;
using IncomeTax.Api.Contracts.Auth.Requests;
using IncomeTax.Api.Contracts.Auth.Responses;
using IncomeTax.Core.Domain;
using IncomeTax.Core.Models;
using IncomeTax.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace IncomeTax.Api.Controllers
{
	[Route("[controller]/[action]")]
	public class AuthController : Controller
	{
		private readonly IMapper _mapper;
		private readonly IService _service;

		public AuthController(IService service, IMapper mapper)
		{
			_service = service;
			_mapper = mapper;
		}

		[HttpPost]
		public async Task<IActionResult> GetRefreshToken([FromBody] RefreshTokenRequest request)
		{
			var result = await _service.Identity.RefreshTokenAsync(request.Token, request.RefreshToken);

			return result.Success ?
					   Ok(_mapper.Map<AuthenticationResult, AuthSuccessResponse>(result)) :
					   BadRequest(_mapper.Map<AuthenticationResult, AuthFailedResponse>(result));
		}

		[HttpPost]
		public async Task<IActionResult> Login([FromBody] LoginRequest request)
		{
			var result = await _service.Identity.LoginAsync(request.Email, request.Password);

			return result.Success ?
					   Ok(_mapper.Map<AuthenticationResult, AuthSuccessResponse>(result)) :
					   BadRequest(_mapper.Map<AuthenticationResult, AuthFailedResponse>(result));
		}

		[HttpPost]
		public async Task<IActionResult> Register([FromBody] RegisterRequest request)
		{
			var person = _mapper.Map<RegisterRequest, Person>(request);

			var result = await _service.Identity.RegisterUserAsync(person);

			if (result.Succeed)
			{
				return Ok(new { result.Message });
			}

			return BadRequest(new
			{
				result.Message,
				result.Errors
			});
		}
	}
}