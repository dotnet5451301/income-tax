using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IncomeTax.Api.Contracts.Insurance;
using IncomeTax.Api.Extensions;
using IncomeTax.Core.Models;
using IncomeTax.Core.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IncomeTax.Api.Controllers
{
	[Route("[controller]/[action]")]
	[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
	public class InsuranceController : Controller
	{
		private readonly IMapper _mapper;
		private readonly IService _service;

		public InsuranceController(IMapper mapper, IService service)
		{
			_mapper = mapper;
			_service = service;
		}

		[HttpPost]
		public async Task<IActionResult> Add([FromBody] InsuranceRequest body, CancellationToken cancellationToken)
		{
			if (!long.TryParse(HttpContext.GetUserId(), out var userId))
			{
				return BadRequest("Invalid request!");
			}
			
			var result = await _service.Insurance.AddAsync(1, _mapper.Map<Insurance>(body));

			return result.Succeed ?
					   Ok(result.Message) :
					   BadRequest(result.Errors);
		}
	}
}