﻿using System.Linq;
using System.Threading.Tasks;
using IncomeTax.Api.Contracts.Responses.Validation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace IncomeTax.Api.Filters
{
	/// <summary>
	///     This is a middleware that filters every request and only validated request get passed. All others are returned
	///     back to the server.
	/// </summary>
	public class ValidationFilter : IAsyncActionFilter
	{
		public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
		{
			if (!context.ModelState.IsValid)
			{
				var errorsInModelState = context.ModelState
												.Where(e => e.Value?.Errors.Count > 0)
												.ToDictionary(kvp => kvp.Key,
															  kvp => kvp.Value?.Errors
																		.Select(x => x.ErrorMessage))
												.ToArray();

				var errorResponse = new ErrorResponse();

				foreach (var error in errorsInModelState)
				{
					foreach (var subError in error.Value)
					{
						var errorModel = new ErrorModel
						{
							FieldName = error.Key,
							Error = subError
						};

						errorResponse.Errors.Add(errorModel);
					}
				}

				context.Result = new BadRequestObjectResult(errorResponse);

				return;
			}

			await next();
		}
	}
}