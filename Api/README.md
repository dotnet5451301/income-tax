# Accounting

## How to develop

### Install dependencies

- [Download](https://download.visualstudio.microsoft.com/download/pr/cf196f2f-f1e2-4f9a-a7ac-546242c431e2/8c386932f4a2f96c3e95c433e4899ec2/dotnet-sdk-8.0.100-osx-arm64.pkg
  ) and install .NET 8.0 SDK
- Docker

### Start Development

- Run `dotnet restore` to install dependencies within project directory
- Start database:
```shell
docker run --name accounting-database -e POSTGRES_USER=accounting -e POSTGRES_PASSWORD=accounting -e POSTGRES_DATABASE=accounting -p 5432:5432 -d postgres
```
- Run `dotnet ef database update` to create database schema
- Run `dotnet run` to start the application