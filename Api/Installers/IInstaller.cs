﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IncomeTax.Api.Installers
{
	public interface IInstaller
	{
		public void InstallServices(IServiceCollection services, IConfiguration configuration);
	}
}