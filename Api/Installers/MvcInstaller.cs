﻿using System;
using System.Text;
using FluentValidation.AspNetCore;
using IncomeTax.Api.Filters;
using IncomeTax.Core.Services;
using IncomeTax.Services;
using IncomeTax.Services.Options;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace IncomeTax.Api.Installers
{
	public class MvcInstaller : IInstaller
	{
		public void InstallServices(IServiceCollection services, IConfiguration configuration)
		{
			services.Configure<IdentityOptions>(options =>
			{
				options.Password.RequireDigit = true;
				options.Password.RequiredLength = 8;
				options.Password.RequireLowercase = true;
				options.Password.RequireUppercase = true;
			});
			
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo
				{
					Version = "v1",
					Title = "Equity Management System",
					Description = "An application to manage your accounts from pencil to aeroplane :D",
					Contact = new OpenApiContact
					{
						Name = "Muhammad Imran Faruqi",
						Email = "mimranfaruqi@gmail.com",
						Url = new Uri("https://github.com/mimranfaruqi")
					}
				});

				c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
				{
					Type = SecuritySchemeType.Http,
					Scheme = "bearer",
					BearerFormat = "JWT",
					In = ParameterLocation.Header,
					Description = "JWT Bearer Token for Authentication / Authorization"
				});
				
				c.AddSecurityRequirement(new OpenApiSecurityRequirement
				{
					{
						new OpenApiSecurityScheme
						{
							Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "Bearer" }
						},
						new[] { "readAccess", "writeAccess" }
					}
				});
			});

			services.AddScoped<IService, Service>();
			services.AddAutoMapper(typeof(Program).Assembly);

			var jwtSettings = new JwtSettings();
			configuration.Bind(nameof(jwtSettings), jwtSettings);
			services.AddSingleton(jwtSettings);

			services.AddControllers();

			services.AddFluentValidationAutoValidation().AddFluentValidationClientsideAdapters();
			
			services.AddMvc(options =>
					{
						options.Filters.Add<ValidationFilter>();
					});

			
			var tokenValidationParameters = new TokenValidationParameters
			{
				ValidateIssuerSigningKey = true,
				IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtSettings.Secret)),
				ValidateIssuer = false,
				ValidateAudience = false,
				RequireExpirationTime = false,
				ValidateLifetime = true
			};

			services.AddSingleton(tokenValidationParameters);

			services.AddAuthentication(x =>
					{
						x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
						x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
						x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
					})
					.AddJwtBearer(x =>
					{
						x.SaveToken = true;
						x.TokenValidationParameters = tokenValidationParameters;
					});

			services.AddAuthorization(options =>
			{
				// TODO: Policies can be added by following pattern. Copied from FinalPublications.
				// options.AddPolicy(nameof(AuthorizeEditorHandler), builder => builder.RequireClaim("CanViewEverything", "true"));
				//options.AddPolicy(nameof(AuthorizeEditorHandler),
				// builder => builder.Requirements.Add(new AuthorizeEditorRequirement()));

				// options.AddPolicy(nameof(AuthorizeAuthorHandler),
				// builder => builder.Requirements.Add(new AuthorizeAuthorRequirement()));
			});
		}
	}
}