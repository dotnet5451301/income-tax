﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IncomeTax.Api.Contracts.RequestModels
{
	public class PersonRequest
	{
		[Required]
		[MinLength(3)]
		public string FirstName { get; set; }

		public string MiddleName { get; set; }

		[Required]
		[MinLength(1)]
		public string LastName { get; set; }

		[Required]
		public DateOnly DateOfBirth { get; set; }

		public string CNIC { get; set; }
		public string IRISPassword { get; set; }
	}
}