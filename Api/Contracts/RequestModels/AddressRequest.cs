using System.ComponentModel.DataAnnotations;

namespace IncomeTax.Api.Contracts.RequestModels
{
	public class AddressRequest
	{
		[Required]
		[MinLength(5)]
		public string AddressLine1 { get; set; }

		public string AddressLine2 { get; set; }

		[Required]
		[MinLength(3)]
		public string City { get; set; }

		[Required]
		public bool IsCity { get; set; }

		[Required]
		[MinLength(3)]
		public string State { get; set; }

		[Required]
		[Range(1, 246)]
		public byte CountryId { get; set; }
	}
}