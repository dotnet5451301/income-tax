﻿using System.ComponentModel.DataAnnotations;

namespace IncomeTax.Api.Contracts.RequestModels
{
	public class UserRequest
	{
		[Required]
		[EmailAddress]
		public string Email { get; set; }

		[Required]
		public string Password { get; set; }

		[Required]
		[Phone]
		public string Phone { get; set; }

		[Required]
		public bool TwoFactorAuth { get; set; }
	}
}