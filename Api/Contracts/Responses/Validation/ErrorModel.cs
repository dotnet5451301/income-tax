﻿namespace IncomeTax.Api.Contracts.Responses.Validation
{
	public class ErrorModel
	{
		public string FieldName { get; set; }
		public string Error { get; set; }
	}
}