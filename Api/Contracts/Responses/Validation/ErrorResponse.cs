﻿using System.Collections.Generic;

namespace IncomeTax.Api.Contracts.Responses.Validation
{
	public class ErrorResponse
	{
		public List<ErrorModel> Errors { get; set; } = new();
	}
}