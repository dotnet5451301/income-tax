﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IncomeTax.Api.Contracts.Auth.Requests
{
	public class RefreshTokenRequest
	{
		[Required]
		public string Token { get; set; }

		[Required]
		public Guid RefreshToken { get; set; }
	}
}