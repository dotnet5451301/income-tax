﻿using System.ComponentModel.DataAnnotations;
using IncomeTax.Api.Contracts.RequestModels;

namespace IncomeTax.Api.Contracts.Auth.Requests
{
	public class RegisterRequest
	{
		[Required]
		public UserRequest User { get; set; }

		[Required]
		public PersonRequest PersonalInformation { get; set; }
	}
}