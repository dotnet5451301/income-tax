﻿using System.ComponentModel.DataAnnotations;

namespace IncomeTax.Api.Contracts.Auth.Requests
{
	public class LoginRequest
	{
		[EmailAddress]
		[Required]
		public string Email { get; set; }

		[Required]
		[MinLength(6)]
		public string Password { get; set; }
	}
}