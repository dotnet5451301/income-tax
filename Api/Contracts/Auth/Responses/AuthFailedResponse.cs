﻿using System.Collections.Generic;

namespace IncomeTax.Api.Contracts.Auth.Responses
{
	public class AuthFailedResponse
	{
		public IDictionary<string, string> Errors { get; set; }
	}
}