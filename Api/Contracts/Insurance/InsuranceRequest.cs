using System;
using System.ComponentModel.DataAnnotations;

namespace IncomeTax.Api.Contracts.Insurance
{
	public class InsuranceRequest
	{
		[Required]
		public string PolicyNo { get; set; }

		[Required]
		public decimal Premium { get; set; }

		[Required]
		public decimal MaturityValue { get; set; }

		[Required]
		public DateOnly MaturityDate { get; set; }

		[Required]
		public bool IsOnName { get; set; }

		[Required]
		public decimal PurchaseValue { get; set; }
		
		[Required]
		public DateOnly PurchaseDate { get; set; }
	}
}