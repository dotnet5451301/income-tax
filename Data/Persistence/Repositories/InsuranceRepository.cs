using IncomeTax.Core.Models;
using IncomeTax.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace IncomeTax.Data.Persistence.Repositories
{
	public class InsuranceRepository : Repository<Insurance>, IInsuranceRepository
	{
		public InsuranceRepository(DbContext context) : base(context) { }
	}
}