﻿using IncomeTax.Core.Models;
using IncomeTax.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace IncomeTax.Data.Persistence.Repositories
{
	public class RefreshTokenRepository : Repository<RefreshToken>, IRefreshTokenRepository
	{
		public RefreshTokenRepository(DbContext context) : base(context) { }
	}
}