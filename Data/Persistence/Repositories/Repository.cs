﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using IncomeTax.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace IncomeTax.Data.Persistence.Repositories
{
	public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
	{
		protected readonly DbContext Context;

		protected Repository(DbContext context) => Context = context;

		public async Task<TEntity?> GetAsync(long id) =>
			await Context.Set<TEntity?>().FindAsync(id);

		public async Task<IEnumerable<TEntity>> GetAllAsync() =>
			// Note that here I've repeated Context.Set<TEntity>() in every method and this is causing
			// too much noise. I could get a reference to the DbSet returned from this method in the 
			// constructor and store it in a private field like _entities. This way, the implementation
			// of our methods would be cleaner:
			// 
			// _entities.ToList();
			// _entities.Where();
			// _entities.SingleOrDefault();
			// 
			// I didn't change it because I wanted the code to look like the videos. But feel free to change
			// this on your own.
			await Context.Set<TEntity>().ToListAsync();

		public async Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate) =>
			await Context.Set<TEntity>().Where(predicate).ToListAsync();

		public async Task<TEntity?> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate) =>
			await Context.Set<TEntity>().SingleOrDefaultAsync(predicate);

		public async Task AddAsync(TEntity entity) =>
			await Context.Set<TEntity>().AddAsync(entity);

		public async Task AddRangeAsync(IEnumerable<TEntity> entities) =>
			await Context.Set<TEntity>().AddRangeAsync(entities);

		public void Remove(TEntity entity) =>
			Context.Set<TEntity>().Remove(entity);

		public void RemoveRange(IEnumerable<TEntity> entities) =>
			Context.Set<TEntity>().RemoveRange(entities);
	}
}