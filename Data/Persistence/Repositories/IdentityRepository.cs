﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IncomeTax.Core.Domain;
using IncomeTax.Core.Models;
using IncomeTax.Core.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace IncomeTax.Data.Persistence.Repositories
{
	public class IdentityRepository : Repository<User>, IIdentityRepository
	{
		private readonly UserManager<User> _userManager;

		public IdentityRepository(DbContext context, UserManager<User> userManager) :
			base(context) => _userManager = userManager;

		private DataContext DataContext => Context as DataContext;

		public async Task CreateUserWithPasswordHash(User user)
		{
			user.PasswordHash = _userManager.PasswordHasher.HashPassword(user, user.PasswordHash);

			user.NormalizedEmail = user.Email.ToUpper();
			user.SecurityStamp = Guid.NewGuid().ToString();
			user.AccessFailedCount = 3;
			user.ConcurrencyStamp = Guid.NewGuid().ToString();

			await DataContext.AddAsync(user);
		}

		public async Task<bool> HasValidPassword(User user, string password) => await _userManager.CheckPasswordAsync(user, password);

		public async Task<Result> AddToRoleAsync(User user, string role)
		{
			var roleInDb = await DataContext.Roles.SingleOrDefaultAsync(x => x.NormalizedName == role.ToUpper());

			if (roleInDb == null)
			{
				return new Result(false, "Oops! Unable to assign role. Status code 1.");
			}

			var userRole = new UserRole
			{
				Role = roleInDb,
				User = user
			};
			
			await DataContext.UserRoles.AddAsync(userRole);
			return new Result(true, "Specified role is assigned!");
		}

		public async Task<Result> AddToRolesAsync(User user, IList<string> roles)
		{
			roles = roles.Select(x => x.ToUpper()).ToList();

			var rolesInDb = await DataContext.Roles.Where(x => roles.Contains(x.NormalizedName)).ToListAsync();

			if (rolesInDb.Count != roles.Count)
			{
				return new Result(false, "Invalid roles provided!");
			}

			await DataContext.UserRoles.AddRangeAsync(rolesInDb.Select(x => new UserRole
			{
				Role = x,
				User = user
			}));
			return new Result(true, "Roles have been assigned!");
		}

		public async Task<IList<string>> GetUserRolesAsync(User user) => await _userManager.GetRolesAsync(user);

		public async Task<IList<Claim>> GetUserClaimsAsync(User user) => await _userManager.GetClaimsAsync(user);

		public async Task<Result> ChangePasswordAsync(User user, string oldPassword, string newPassword)
		{
			var result = await _userManager.ChangePasswordAsync(user, oldPassword, newPassword);

			return result.Succeeded
					   ? new Result(true, "Password Changed Successfully!")
					   : new Result(false, "Unable to change password!", result.Errors.Select(x => new Dictionary<string, string> { { x.Code, x.Description } }).ToList());
		}
	}
}