﻿using IncomeTax.Core.Models;
using IncomeTax.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace IncomeTax.Data.Persistence.Repositories
{
	public class PersonRepository : Repository<Person>, IPersonRepository
	{
		public PersonRepository(DbContext context) : base(context) { }
	}
}