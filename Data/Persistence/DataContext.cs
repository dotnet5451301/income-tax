﻿using IncomeTax.Core.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace IncomeTax.Data.Persistence
{
	public class DataContext : IdentityDbContext<User, Role, long, UserClaim, UserRole,
		UserLogin, RoleClaim, UserToken>
	{
		public DataContext(DbContextOptions options) : base(options) { }

		public virtual DbSet<AccountType> AccountTypes { get; set; }
		public virtual DbSet<Address> Addresses { get; set; }
		public virtual DbSet<AreaType> AreaTypes { get; set; }
		public virtual DbSet<Asset> Assets { get; set; }
		public virtual DbSet<Balance> Balances { get; set; }
		public virtual DbSet<BankAccount> BankAccounts { get; set; }
		public virtual DbSet<CashEquivalent> CashEquivalents { get; set; }
		public virtual DbSet<CashNature> CashNatures { get; set; }
		public virtual DbSet<Country> Countries { get; set; }
		public virtual DbSet<CryptoCurrency> CryptoCurrencies { get; set; }
		public virtual DbSet<CurrentAsset> CurrentAssets { get; set; }
		public virtual DbSet<Debenture> Debentures { get; set; }
		public virtual DbSet<Debt> Debts { get; set; }
		public virtual DbSet<DebtNature> DebtNatures { get; set; }
		public virtual DbSet<Expense> Expenses { get; set; }
		public virtual DbSet<ExpenseType> ExpenseTypes { get; set; }
		public virtual DbSet<FixedAsset> FixedAssets { get; set; }
		public virtual DbSet<Insurance> Insurances { get; set; }
		public virtual DbSet<Liability> Liabilities { get; set; }
		public virtual DbSet<LiabilityType> LiabilityTypes { get; set; }
		public virtual DbSet<OtherAssetType> OtherAssetTypes { get; set; }
		public virtual DbSet<OtherCurrentAsset> OtherCurrentAssets { get; set; }
		public virtual DbSet<OtherInvestment> OtherInvestments { get; set; }
		public virtual DbSet<OtherInvestmentType> OtherInvestmentTypes { get; set; }
		public virtual DbSet<Person> Persons { get; set; }
		public virtual DbSet<Property> Properties { get; set; }
		public virtual DbSet<RefreshToken> RefreshTokens { get; set; }
		public virtual DbSet<Vehicle> Vehicles { get; set; }
		public virtual DbSet<VehicleType> VehicleTypes { get; set; }

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);
			builder.ApplyConfigurationsFromAssembly(GetType().Assembly);
		}
	}
}