﻿using System;
using System.Threading.Tasks;
using IncomeTax.Core;
using IncomeTax.Core.Models;
using IncomeTax.Core.Repositories;
using IncomeTax.Data.Persistence.Repositories;
using Microsoft.AspNetCore.Identity;

namespace IncomeTax.Data.Persistence
{
	public class UnitOfWork : IUnitOfWork
	{
		private readonly DataContext _context;

		public UnitOfWork(DataContext context, UserManager<User> userManager)
		{
			_context = context;
			Persons = new PersonRepository(_context);
			Users = new IdentityRepository(_context, userManager);
			RefreshTokens = new RefreshTokenRepository(_context);
			Insurances = new InsuranceRepository(_context);
		}

		public IPersonRepository Persons { get; }

		public IInsuranceRepository Insurances { get; }

		public IIdentityRepository Users { get; }
		public IRefreshTokenRepository RefreshTokens { get; }

		public async Task<int> CompleteAsync() => await _context.SaveChangesAsync();

		public void Dispose()
		{
			_context.Dispose();
			GC.SuppressFinalize(this);
		}
	}
}