using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class CountryConfiguration : IEntityTypeConfiguration<Country>
	{
		public void Configure(EntityTypeBuilder<Country> builder)
		{
			builder.ToTable("Countries");

			builder.HasKey(x => x.Id);

			builder.Property(x => x.Id).HasColumnType("smallint").UseIdentityByDefaultColumn();
			builder.Property(x => x.Name).IsRequired();

			builder.HasMany(x => x.Addresses)
				   .WithOne(x => x.Country)
				   .HasForeignKey(x => x.CountryId)
				   .OnDelete(DeleteBehavior.Restrict)
				   .IsRequired();
		}
	}
}