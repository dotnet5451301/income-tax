using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class OtherInvestmentConfiguration : IEntityTypeConfiguration<OtherInvestment>
	{
		public void Configure(EntityTypeBuilder<OtherInvestment> builder)
		{
			builder.ToTable("OtherInvestments");

			builder.Property(x => x.Description).IsRequired();
			builder.Property(x => x.AccountNo).IsRequired();
			builder.Property(x => x.InvestedInCompany).IsRequired();

			builder.HasOne(x => x.Type)
				   .WithMany()
				   .HasForeignKey(x => x.TypeId)
				   .OnDelete(DeleteBehavior.Restrict)
				   .IsRequired();
		}
	}
}