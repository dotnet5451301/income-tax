using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class DebentureConfiguration : IEntityTypeConfiguration<Debenture>
	{
		public void Configure(EntityTypeBuilder<Debenture> builder)
		{
			builder.ToTable("Debentures");

			builder.Property(x => x.DebentureNo).IsRequired();
			
			builder.HasIndex(x => x.DebentureNo).IsUnique();
		}
	}
}