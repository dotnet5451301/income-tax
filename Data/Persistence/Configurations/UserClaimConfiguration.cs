using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class UserClaimConfiguration : IEntityTypeConfiguration<UserClaim>
	{
		public void Configure(EntityTypeBuilder<UserClaim> builder)
		{
			builder.ToTable("UserClaims");
			builder.HasKey(x => x.Id);

			builder.Property(x => x.Id).HasColumnType("BIGINT").UseIdentityAlwaysColumn();
		}
	}
}