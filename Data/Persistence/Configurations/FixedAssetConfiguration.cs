using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class FixedAssetConfiguration : IEntityTypeConfiguration<FixedAsset>
	{
		public void Configure(EntityTypeBuilder<FixedAsset> builder)
		{
			builder.ToTable("FixedAssets");
		}
	}
}