using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class ExpenseConfiguration : IEntityTypeConfiguration<Expense>
	{
		public void Configure(EntityTypeBuilder<Expense> builder)
		{
			builder.ToTable("Expenses");

			builder.HasKey(x => x.Id);
			builder.Property(x => x.Id).HasColumnType("BIGINT").UseIdentityAlwaysColumn();

			builder.HasOne(x => x.Type)
				   .WithMany()
				   .HasForeignKey(x => x.ExpenseTypeId)
				   .OnDelete(DeleteBehavior.Restrict)
				   .IsRequired();
		}
	}
}