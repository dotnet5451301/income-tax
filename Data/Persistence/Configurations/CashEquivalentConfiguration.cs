using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class CashEquivalentConfiguration : IEntityTypeConfiguration<CashEquivalent>
	{
		public void Configure(EntityTypeBuilder<CashEquivalent> builder)
		{
			builder.ToTable("CashEquivalents");

			builder.HasOne(x => x.Nature)
				   .WithMany()
				   .HasForeignKey(x => x.NatureId)
				   .OnDelete(DeleteBehavior.Restrict)
				   .IsRequired();
		}
	}
}