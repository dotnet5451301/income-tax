using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class UserLoginConfiguration : IEntityTypeConfiguration<UserLogin>
	{
		public void Configure(EntityTypeBuilder<UserLogin> builder)
		{
			builder.ToTable("UserLogins");
			builder.HasKey(x => new { x.LoginProvider, x.ProviderKey });

			builder.Property(x => x.ProviderKey).HasMaxLength(128);
			builder.Property(x => x.LoginProvider).HasMaxLength(128);
		}
	}
}