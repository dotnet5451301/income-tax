using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class CryptoCurrencyConfiguration : IEntityTypeConfiguration<CryptoCurrency>
	{
		public void Configure(EntityTypeBuilder<CryptoCurrency> builder)
		{
			builder.ToTable("CryptoCurrencies");

			builder.Property(x => x.CryptoName).IsRequired();
		}
	}
}