using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class BankAccountConfiguration : IEntityTypeConfiguration<BankAccount>
	{
		public void Configure(EntityTypeBuilder<BankAccount> builder)
		{
			builder.ToTable("BankAccounts");

			builder.Property(x => x.Branch).IsRequired();
			builder.Property(x => x.AccountNo).IsRequired();

			builder.Property(x => x.Bank).IsRequired();

			builder.HasOne(x => x.Type)
				   .WithMany()
				   .HasForeignKey(x => x.AccountTypeId)
				   .OnDelete(DeleteBehavior.Restrict)
				   .IsRequired();
			
			builder.HasOne(x => x.Address)
				   .WithOne()
				   .HasForeignKey<BankAccount>(x => x.AddressId)
				   .OnDelete(DeleteBehavior.Cascade)
				   .IsRequired();
		}
	}
}