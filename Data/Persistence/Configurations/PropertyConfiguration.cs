using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class PropertyConfiguration : IEntityTypeConfiguration<Property>
	{
		public void Configure(EntityTypeBuilder<Property> builder)
		{
			builder.ToTable("Properties");

			builder.HasOne(x => x.AreaType)
				   .WithMany()
				   .HasForeignKey(x => x.AreaTypeId)
				   .OnDelete(DeleteBehavior.Restrict)
				   .IsRequired();

			builder.HasOne(x => x.Address)
				   .WithOne()
				   .HasForeignKey<Property>(x => x.AddressId)
				   .OnDelete(DeleteBehavior.Cascade)
				   .IsRequired();
		}
	}
}