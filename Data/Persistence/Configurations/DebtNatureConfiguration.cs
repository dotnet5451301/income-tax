using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class DebtNatureConfiguration : IEntityTypeConfiguration<DebtNature>
	{
		public void Configure(EntityTypeBuilder<DebtNature> builder)
		{
			builder.ToTable("DebtNatures");

			builder.HasKey(x => x.Id);

			builder.Property(x => x.Id).HasColumnType("smallint").UseIdentityByDefaultColumn();
			builder.Property(x => x.Name).IsRequired();
		}
	}
}