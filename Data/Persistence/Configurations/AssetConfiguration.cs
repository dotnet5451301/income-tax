using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class AssetConfiguration : IEntityTypeConfiguration<Asset>
	{
		public void Configure(EntityTypeBuilder<Asset> builder)
		{
			builder.ToTable("Assets");
			builder.HasKey(x => x.Id);

			builder.Property(x => x.Id).HasColumnType("BIGINT").UseIdentityAlwaysColumn();
		}
	}
}