using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class VehicleConfiguration : IEntityTypeConfiguration<Vehicle>
	{
		public void Configure(EntityTypeBuilder<Vehicle> builder)
		{
			builder.ToTable("Vehicles");

			builder.Property(x => x.Brand).IsRequired();
			builder.Property(x => x.Make).IsRequired();
			builder.Property(x => x.Model).IsRequired();
			builder.Property(x => x.RegistrationNo).IsRequired();

			builder.HasOne(x => x.VehicleType)
				   .WithMany()
				   .HasForeignKey(x => x.VehicleTypeId)
				   .OnDelete(DeleteBehavior.Restrict)
				   .IsRequired();
		}
	}
}