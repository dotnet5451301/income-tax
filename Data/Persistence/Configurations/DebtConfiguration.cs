using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class DebtConfiguration : IEntityTypeConfiguration<Debt>
	{
		public void Configure(EntityTypeBuilder<Debt> builder)
		{
			builder.ToTable("Debts");

			builder.Property(x => x.Name).IsRequired();
			builder.Property(x => x.RegistrationNo).IsRequired();

			builder.HasOne(x => x.Nature)
				   .WithMany()
				   .HasForeignKey(x => x.NatureId)
				   .OnDelete(DeleteBehavior.Restrict)
				   .IsRequired();
		}
	}
}