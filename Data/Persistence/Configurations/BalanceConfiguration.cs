using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class BalanceConfiguration : IEntityTypeConfiguration<Balance>
	{
		public void Configure(EntityTypeBuilder<Balance> builder)
		{
			builder.ToTable("Balances");

			builder.Property(x => x.Date).HasColumnType("date");
			
			builder.HasKey(x => new { x.CurrentAssetId, x.Date, x.IsProfit });
		}
	}
}