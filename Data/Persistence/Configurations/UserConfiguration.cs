using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class UserConfiguration : IEntityTypeConfiguration<User>
	{
		public void Configure(EntityTypeBuilder<User> builder)
		{
			builder.ToTable("Users");

			builder.HasKey(x => x.Id);

			builder.Property(x => x.ConcurrencyStamp).IsConcurrencyToken();
			builder.Property(x => x.Id).HasColumnType("BIGINT").UseIdentityAlwaysColumn();
			builder.Property(x => x.NormalizedEmail).HasComputedColumnSql("UPPER(\"Email\")", true);
			builder.Property(x => x.Email).HasMaxLength(256).HasConversion(x => x.ToLower(), x => x);
			builder.Property(x => x.PasswordHash).IsRequired();
			builder.Property(x => x.UserName).HasMaxLength(256);
			builder.Property(x => x.NormalizedUserName).HasComputedColumnSql("UPPER(\"UserName\")", true);

			builder.HasIndex(x => x.NormalizedEmail).HasDatabaseName("EmailIndex");
			builder.HasIndex(x => x.NormalizedUserName).HasDatabaseName("UserNameIndex");
			
			builder.HasMany(x => x.Claims)
				   .WithOne(x => x.User)
				   .HasForeignKey(x => x.UserId)
				   .OnDelete(DeleteBehavior.Cascade)
				   .IsRequired();

			builder.HasMany(x => x.Logins)
				   .WithOne(x => x.User)
				   .HasForeignKey(x => x.UserId)
				   .OnDelete(DeleteBehavior.Cascade)
				   .IsRequired();

			builder.HasMany(x => x.Roles)
				   .WithOne(x => x.User)
				   .HasForeignKey(x => x.UserId)
				   .OnDelete(DeleteBehavior.Cascade)
				   .IsRequired();

			builder.HasMany(x => x.Tokens)
				   .WithOne(x => x.User)
				   .HasForeignKey(x => x.UserId)
				   .OnDelete(DeleteBehavior.Cascade)
				   .IsRequired();
		}
	}
}