using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class VehicleTypeConfiguration : IEntityTypeConfiguration<VehicleType>
	{
		public void Configure(EntityTypeBuilder<VehicleType> builder)
		{
			builder.ToTable("VehicleTypes");

			builder.HasKey(x => x.Id);

			builder.Property(x => x.Id).HasColumnType("smallint").UseIdentityByDefaultColumn();
			builder.Property(x => x.Name).IsRequired();
		}
	}
}