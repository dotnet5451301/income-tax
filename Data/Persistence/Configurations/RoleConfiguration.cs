using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class RoleConfiguration : IEntityTypeConfiguration<Role>
	{
		public void Configure(EntityTypeBuilder<Role> builder)
		{
			builder.ToTable("Roles");

			builder.HasKey(x => x.Id);

			builder.Property(x => x.Id).HasColumnType("INT").UseIdentityByDefaultColumn();
			builder.Property(x => x.Name).HasMaxLength(20).IsRequired();
			builder.Property(x => x.NormalizedName).HasComputedColumnSql("UPPER(\"Name\")", true);
			builder.Property(x => x.ConcurrencyStamp).IsConcurrencyToken();

			builder.HasIndex(x => x.NormalizedName).HasDatabaseName("RoleNameIndex");
			
			builder.HasMany(x => x.Users)
				   .WithOne(x => x.Role)
				   .HasForeignKey(x => x.RoleId)
				   .OnDelete(DeleteBehavior.Restrict)
				   .IsRequired();
			
			builder.HasMany(x => x.Claims)
				   .WithOne(x => x.Role)
				   .HasForeignKey(x => x.RoleId)
				   .OnDelete(DeleteBehavior.Restrict)
				   .IsRequired();
		}
	}
}