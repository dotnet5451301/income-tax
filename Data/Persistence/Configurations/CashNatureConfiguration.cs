using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class CashNatureConfiguration : IEntityTypeConfiguration<CashNature>
	{
		public void Configure(EntityTypeBuilder<CashNature> builder)
		{
			builder.ToTable("CashNatures");

			builder.HasKey(x => x.Id);
			builder.Property(x => x.Id).HasColumnType("smallint").UseIdentityByDefaultColumn();
			builder.Property(x => x.Name).IsRequired();
		}
	}
}