using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class RoleClaimConfiguration : IEntityTypeConfiguration<RoleClaim>
	{
		public void Configure(EntityTypeBuilder<RoleClaim> builder)
		{
			builder.ToTable("RoleClaims");
			builder.HasKey(x => x.Id);

			builder.Property(x => x.Id).HasColumnType("BIGINT").UseIdentityAlwaysColumn();
		}
	}
}