using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class LiabilityConfiguration : IEntityTypeConfiguration<Liability>
	{
		public void Configure(EntityTypeBuilder<Liability> builder)
		{
			builder.ToTable("Liabilities");

			builder.HasKey(x => x.Id);

			builder.Property(x => x.Id).HasColumnType("BIGINT").UseIdentityAlwaysColumn();

			builder.Property(x => x.RegistrationNo).IsRequired();
			builder.Property(x => x.Name).IsRequired();

			builder.HasOne(x => x.Type)
				   .WithMany()
				   .HasForeignKey(x => x.LiabilityTypeId)
				   .OnDelete(DeleteBehavior.Restrict)
				   .IsRequired();
		}
	}
}