﻿using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class PersonConfiguration : IEntityTypeConfiguration<Person>
	{
		public void Configure(EntityTypeBuilder<Person> builder)
		{
			builder.HasKey(x => x.Id);

			builder.Property(x => x.Id).HasColumnType("BIGINT").UseIdentityAlwaysColumn();
			builder.Property(x => x.FirstName).IsRequired();
			builder.Property(x => x.LastName).IsRequired();
			builder.Property(x => x.CNIC).IsRequired();
			builder.Property(x => x.IRISPasswordHash);

			builder.HasIndex(x => x.CNIC).IsUnique();

			builder.Ignore(x => x.FullName);

			builder.HasOne(x => x.User)
				   .WithOne()
				   .HasForeignKey<Person>(x => x.UserId)
				   .OnDelete(DeleteBehavior.SetNull);

			builder.HasMany(x => x.Assets)
				   .WithOne(x => x.Person)
				   .HasForeignKey(x => x.PersonId)
				   .OnDelete(DeleteBehavior.Cascade)
				   .IsRequired();

			builder.HasMany(x => x.Expenses)
				   .WithOne(x => x.Person)
				   .HasForeignKey(x => x.PersonId)
				   .OnDelete(DeleteBehavior.Cascade)
				   .IsRequired();
		}
	}
}