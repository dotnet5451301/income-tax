using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class OtherInvestmentTypeConfiguration : IEntityTypeConfiguration<OtherInvestmentType>
	{
		public void Configure(EntityTypeBuilder<OtherInvestmentType> builder)
		{
			builder.ToTable("OtherInvestmentTypes");

			builder.HasKey(x => x.Id);

			builder.Property(x => x.Id).HasColumnType("smallint").UseIdentityByDefaultColumn();
			builder.Property(x => x.Name).IsRequired();
		}
	}
}