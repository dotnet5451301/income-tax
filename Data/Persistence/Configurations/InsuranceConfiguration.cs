using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class InsuranceConfiguration : IEntityTypeConfiguration<Insurance>
	{
		public void Configure(EntityTypeBuilder<Insurance> builder)
		{
			builder.ToTable("Insurances");

			builder.Property(x => x.PolicyNo).IsRequired();

			builder.HasIndex(x => x.PolicyNo).IsUnique();
		}
	}
}