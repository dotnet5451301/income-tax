using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class OtherCurrentAssetConfiguration : IEntityTypeConfiguration<OtherCurrentAsset>
	{
		public void Configure(EntityTypeBuilder<OtherCurrentAsset> builder)
		{
			builder.ToTable("OtherCurrentAssets");

			builder.Property(x => x.Description).IsRequired();

			builder.HasOne(x => x.OtherAssetType)
				   .WithMany()
				   .HasForeignKey(x => x.OtherAssetTypeId)
				   .OnDelete(DeleteBehavior.Restrict)
				   .IsRequired();
		}
	}
}