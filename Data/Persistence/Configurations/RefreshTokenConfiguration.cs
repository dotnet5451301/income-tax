﻿using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class RefreshTokenConfiguration : IEntityTypeConfiguration<RefreshToken>
	{
		public void Configure(EntityTypeBuilder<RefreshToken> builder)
		{
			builder.HasKey(x => x.Token);

			builder.Property(x => x.CreationDate);
			builder.Property(x => x.ExpiryDate);

			builder.HasOne(u => u.User)
				   .WithMany()
				   .HasForeignKey(u => u.UserId)
				   .OnDelete(DeleteBehavior.Cascade)
				   .IsRequired();
		}
	}
}