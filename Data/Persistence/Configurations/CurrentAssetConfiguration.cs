using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class CurrentAssetConfiguration : IEntityTypeConfiguration<CurrentAsset>
	{
		public void Configure(EntityTypeBuilder<CurrentAsset> builder)
		{
			builder.ToTable("CurrentAssets");

			builder.HasMany(x => x.Balances)
				   .WithOne(x => x.Asset)
				   .HasForeignKey(x => x.CurrentAssetId)
				   .OnDelete(DeleteBehavior.Cascade);
		}
	}
}