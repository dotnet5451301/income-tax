using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class AreaTypeConfiguration : IEntityTypeConfiguration<AreaType>
	{
		public void Configure(EntityTypeBuilder<AreaType> builder)
		{
			builder.ToTable("AreaTypes");

			builder.HasKey(x => x.Id);
			builder.Property(x => x.Id).HasColumnType("smallint").UseIdentityByDefaultColumn();
			
			builder.Property(x => x.Name).IsRequired();
		}
	}
}