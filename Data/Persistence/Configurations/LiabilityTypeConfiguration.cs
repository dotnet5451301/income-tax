using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class LiabilityTypeConfiguration : IEntityTypeConfiguration<LiabilityType>
	{
		public void Configure(EntityTypeBuilder<LiabilityType> builder)
		{
			builder.ToTable("LiabilityTypes");

			builder.HasKey(x => x.Id);
			builder.Property(x => x.Id).HasColumnType("smallint").UseIdentityByDefaultColumn();
			builder.Property(x => x.Name).IsRequired();
		}
	}
}