using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class AddressConfiguration : IEntityTypeConfiguration<Address>
	{
		public void Configure(EntityTypeBuilder<Address> builder)
		{
			builder.ToTable("Addresses");

			builder.HasKey(x => x.Id);
			builder.Property(x => x.Id).HasColumnType("BIGINT").UseIdentityAlwaysColumn();

			builder.Property(x => x.StreetOne).IsRequired();
		}
	}
}