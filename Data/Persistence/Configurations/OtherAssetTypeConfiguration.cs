using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class OtherAssetTypeConfiguration : IEntityTypeConfiguration<OtherAssetType>
	{
		public void Configure(EntityTypeBuilder<OtherAssetType> builder)
		{
			builder.ToTable("OtherAssetTypes");

			builder.HasKey(x => x.Id);

			builder.Property(x => x.Id).HasColumnType("smallint").UseIdentityByDefaultColumn();
			builder.Property(x => x.Name).IsRequired();
		}
	}
}