using IncomeTax.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IncomeTax.Data.Persistence.Configurations
{
	public class ExpenseTypeConfiguration : IEntityTypeConfiguration<ExpenseType>
	{
		public void Configure(EntityTypeBuilder<ExpenseType> builder)
		{
			builder.ToTable("ExpensesTypes");

			builder.HasKey(x => x.Id);

			builder.Property(x => x.Id).HasColumnType("smallint").UseIdentityByDefaultColumn();
			builder.Property(x => x.Name).IsRequired();
		}
	}
}