﻿using IncomeTax.Core;
using IncomeTax.Core.Services;
using IncomeTax.Services.Options;
using Microsoft.IdentityModel.Tokens;

namespace IncomeTax.Services
{
	public class Service : IService
	{
		public Service(IUnitOfWork unitOfWork, JwtSettings jwtSettings, TokenValidationParameters tokenValidationParameters)
		{
			Identity = new IdentityService(unitOfWork, jwtSettings, tokenValidationParameters);
			Insurance = new InsuranceService(unitOfWork);
		}

		public IIdentityService Identity { get; }

		public IInsuranceService Insurance { get; }
	}
}