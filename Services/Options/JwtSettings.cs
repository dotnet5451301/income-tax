﻿using System;

namespace IncomeTax.Services.Options
{
	public class JwtSettings
	{
		public string Secret { get; set; }
		public TimeSpan TokenLifetime { get; set; }
	}
}