using System.Collections.Generic;
using System.Threading.Tasks;
using IncomeTax.Core;
using IncomeTax.Core.Domain;
using IncomeTax.Core.Models;
using IncomeTax.Core.Services;

namespace IncomeTax.Services
{
	public class InsuranceService : IInsuranceService
	{
		private readonly IUnitOfWork _unitOfWork;
		
		public InsuranceService(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public async Task<Result> AddAsync(long userId, Insurance insurance)
		{
			var person = await _unitOfWork.Persons.SingleOrDefaultAsync(x => x.UserId == userId);

			if (person == null)
			{
				return new Result(false, string.Empty, new List<Dictionary<string, string>>
				{
					new() {{ "Error", "No such user exists!" } }
				});
			}
			
			insurance.PersonId = person.Id;
			
			await _unitOfWork.Insurances.AddAsync(insurance);
			var res = await _unitOfWork.CompleteAsync();

			return res > 0 ?
				new Result(true, "Insurance has been registered successfully.") :
				new Result(false, "Unable to add Insurance.");
		}
	}
}