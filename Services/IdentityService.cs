﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using IncomeTax.Core;
using IncomeTax.Core.Domain;
using IncomeTax.Core.Models;
using IncomeTax.Core.Services;
using IncomeTax.Services.Options;
using Microsoft.IdentityModel.Tokens;

namespace IncomeTax.Services
{
	public class IdentityService : IIdentityService
	{
		private readonly JwtSettings _jwtSettings;
		private readonly TokenValidationParameters _tokenValidationParameters;
		private readonly IUnitOfWork _unitOfWork;

		public IdentityService(IUnitOfWork unitOfWork, JwtSettings jwtSettings, TokenValidationParameters tokenValidationParameters)
		{
			_unitOfWork = unitOfWork;
			_jwtSettings = jwtSettings;
			_tokenValidationParameters = tokenValidationParameters;
		}

		public async Task<AuthenticationResult> LoginAsync(string email, string password)
		{
			var user = await _unitOfWork.Users.SingleOrDefaultAsync(x => x.NormalizedEmail == email.ToUpper());

			if ((user == null) || !await _unitOfWork.Users.HasValidPassword(user, password))
			{
				return new AuthenticationResult
				{
					Success = false,
					Errors = new Dictionary<string, string> { { "1", "Invalid Email or password!" } }
				};
			}

			return await GenerateAuthenticationResultForUserAsync(user);
		}

		public async Task<AuthenticationResult> RefreshTokenAsync(string token, Guid refreshToken)
		{
			var validatedToken = GetPrincipalFromToken(token);

			if (validatedToken == null)
			{
				return new AuthenticationResult
				{
					Errors = new Dictionary<string, string> { { "Error", "Invalid Token!" } },
					Success = false
				};
			}

			var expiryDateUnix = long.Parse(validatedToken.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Exp).Value);
			var expiryDateTimeUtc = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(expiryDateUnix);

			if (expiryDateTimeUtc > DateTime.UtcNow)
			{
				return new AuthenticationResult
				{
					Errors = new Dictionary<string, string> { { "Error", "This token hasn't expired yet." } },
					Success = false
				};
			}

			var jti = validatedToken.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Jti).Value;

			var storedRefreshToken = await _unitOfWork.RefreshTokens.SingleOrDefaultAsync(x => x.Token == refreshToken);

			if (storedRefreshToken == null)
			{
				return new AuthenticationResult
				{
					Errors = new Dictionary<string, string> { { "Error", "This refresh token does not exist." } },
					Success = false
				};
			}

			if (DateTime.UtcNow > storedRefreshToken.ExpiryDate)
			{
				return new AuthenticationResult
				{
					Errors = new Dictionary<string, string> { { "Error", "This refresh token has expired." } },
					Success = false
				};
			}

			if (storedRefreshToken.Invalidated)
			{
				return new AuthenticationResult
				{
					Errors = new Dictionary<string, string> { { "Error", "Refresh token has been invalidated." } },
					Success = false
				};
			}

			if (storedRefreshToken.JwtId != jti)
			{
				return new AuthenticationResult
				{
					Errors = new Dictionary<string, string> { { "Error", "This refresh token does not match this JWT." } },
					Success = false
				};
			}

			storedRefreshToken.Used = true;
			await _unitOfWork.CompleteAsync();

			if (!long.TryParse(validatedToken.Claims.Single(x => x.Type == "id").Value, out var id))
			{
				return new AuthenticationResult
				{
					Success = false,
					Errors = new Dictionary<string, string> { { "Error", "Something went wrong." } }
				};
			}

			var user = await _unitOfWork.Users.SingleOrDefaultAsync(x => x.Id == id);

			return await GenerateAuthenticationResultForUserAsync(user);
		}

		public async Task<Result> RegisterUserAsync(Person person)
		{
			var userInDb = await _unitOfWork.Users.SingleOrDefaultAsync(x => x.NormalizedEmail == person.User.Email.ToUpper());

			if (userInDb != null)
			{
				return new Result(false, "User already exists!");
			}

			await _unitOfWork.Persons.AddAsync(person);
			await _unitOfWork.Users.CreateUserWithPasswordHash(person.User);
			var result = await _unitOfWork.Users.AddToRoleAsync(person.User, nameof(Core.Enums.Role.Client));

			if (!result.Succeed)
			{
				return result;
			}
			
			var res = await _unitOfWork.CompleteAsync();

			return res > 0 ?
					   new Result(true, "User successfully registered!") :
					   new Result(false, "Unable to register User!");
		}

		public async Task<Result> ChangePasswordAsync(User user, string oldPassword, string newPassword)
		{
			//TODO: Error condition must be logged
			if (string.IsNullOrEmpty(newPassword) || string.IsNullOrEmpty(oldPassword) || (user == null))
			{
				return new Result(false, "Given new password cannot be null or empty!");
			}

			return await _unitOfWork.Users.ChangePasswordAsync(user, oldPassword, newPassword);
		}

		private ClaimsPrincipal GetPrincipalFromToken(string token)
		{
			var tokenHandler = new JwtSecurityTokenHandler();

			try
			{
				var tokenValidationParameters = _tokenValidationParameters.Clone();
				tokenValidationParameters.ValidateLifetime = false;
				var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out var validatedToken);

				return (!IsJwtWithValidSecurityAlgorithm(validatedToken) ? null : principal) ?? new ClaimsPrincipal();
			}
			catch
			{
				return null;
			}
		}

		private bool IsJwtWithValidSecurityAlgorithm(SecurityToken validatedToken) =>
			validatedToken is JwtSecurityToken jwtSecurityToken && jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase);

		private async Task<AuthenticationResult> GenerateAuthenticationResultForUserAsync(User user)
		{
			var userRoles = await _unitOfWork.Users.GetUserRolesAsync(user);

			var tokenHandler = new JwtSecurityTokenHandler();
			var key = Encoding.ASCII.GetBytes(_jwtSettings.Secret);

			var claims = new List<Claim>
			{
				new(JwtRegisteredClaimNames.Sub, user.Email),
				new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
				new(JwtRegisteredClaimNames.Email, user.Email),
				new("id", user.Id.ToString())
			};

			var userClaims = await _unitOfWork.Users.GetUserClaimsAsync(user);
			claims.AddRange(userClaims);
			claims.AddRange(userRoles.Select(role => new Claim(ClaimTypes.Role, role)));

			var tokenDescriptor = new SecurityTokenDescriptor
			{
				Subject = new ClaimsIdentity(claims),
				Expires = DateTime.UtcNow.Add(_jwtSettings.TokenLifetime),
				SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
			};

			var token = tokenHandler.CreateToken(tokenDescriptor);

			var refreshToken = new RefreshToken
			{
				JwtId = token.Id,
				Token = Guid.NewGuid(),
				UserId = user.Id,
				CreationDate = DateTime.UtcNow,
				ExpiryDate = DateTime.UtcNow.AddDays(10)
			};

			await _unitOfWork.RefreshTokens.AddAsync(refreshToken);
			var list = (await _unitOfWork.RefreshTokens.FindAsync(x => (x.UserId == user.Id) && !x.Used)).ToList();

			if (list.Any())
			{
				list.ForEach(x =>
				{
					x.Used = true;
				});
			}

			await _unitOfWork.CompleteAsync();

			return new AuthenticationResult
			{
				Token = tokenHandler.WriteToken(token),
				RefreshToken = refreshToken.Token,
				Success = true
			};
		}
	}
}